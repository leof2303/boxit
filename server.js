/**
 * Created by leonardo on 23/04/16.
 */
var express = require('express'),
    app = express();
var soap = require('soap');
var bodyParser = require('body-parser');
var url = 'http://200.62.34.16/SF.GrupoAuraIntegracionPrueba/?wsdl';
var port = process.env.PORT || 8080;

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Method', 'GET, POST');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
    next();
});
var usersRouter = express.Router();

usersRouter.route('/getplataformas')
    .post(function (req, res) {
        var args = {};
        args["CustomerUser"] = req.body.CustomerUser;
        args["CustomerPassword"] = req.body.CustomerPassword;

        soap.createClient(url, function (err, client) {
            client.GetPlataformas(args, function (err, result) {
                if(result.GetPlataformasResult.Data.Rows.length == undefined){
                    res.json(result.GetPlataformasResult.Data.Rows.attributes.Message);
                }else{
                    res.json(result.GetPlataformasResult.Data.Rows);
                }

            })
        })
    });
usersRouter.route('/insertuserboxit')
    .post(function (req,res) {

});
app.use('/users', usersRouter);
app.listen(port);
console.log('Magic happens on port ' + port);